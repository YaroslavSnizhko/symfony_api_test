<?php


namespace App\Services;

use App\Entity\Status;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;


class BookService
{
    /**
     * @var BookRepository
     */
    private $book;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(BookRepository $book, EntityManagerInterface $em)
    {
        $this->book = $book;
        $this->em = $em;
    }

    public function changeStatusById($id, $status): void
    {
        $book = $this->book->find($id);
        $book->setStatus($status);
        $this->em->flush();
    }
}