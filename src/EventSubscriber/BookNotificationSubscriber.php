<?php

namespace App\EventSubscriber;

use App\Entity\Book;
use App\Events\BookStatusEvent;
use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class BookNotificationSubscriber implements EventSubscriberInterface
{

    private $translator;
    private $urlGenerator;
    private $sender;


    public static function getSubscribedEvents(): array
    {
        return [
            BookStatusEvent::class => 'onStatusUpdate',
        ];
    }

    public function onStatusUpdate(BookStatusEvent $event): void
    {


        $email = (new NotificationEmail())
            ->from('snizhko@hotmail.com')
            ->to('snizhko@hotmail.com')
            ->subject('My first notification email via Symfony')
            ->action('More info?', 'https://example.com/')
            ->importance(NotificationEmail::IMPORTANCE_HIGH);

    }
}
