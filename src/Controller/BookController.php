<?php

namespace App\Controller;

use App\Entity\Book;
use App\Events\BookStatusEvent;
use App\Events\CommentCreatedEvent;
use App\Repository\BookRepository;
use App\Services\BookService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
    /**
     * @Route("/book", name="book")
     */
    public function index()
    {
        return $this->render('book/index.html.twig', [
            'controller_name' => 'BookController',
        ]);
    }

    /**
     * @Route("/book/change-status/{id}", name="change_status")
     */
    public function changeStatus($id, Request $request, EventDispatcherInterface $eventDispatcher): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $bookRepository = $this->getDoctrine()->getRepository(Book::class);
        $bookService = new BookService($bookRepository, $entityManager);
        $bookService->changeStatusById($id, $request->get('status'));
        $eventDispatcher->dispatch(new BookStatusEvent(new Book()));

        return new JsonResponse(['status' => true], JsonResponse::HTTP_CREATED);
    }
}
